package com.ushakova.tm.comparator;

import com.ushakova.tm.api.entity.IHasCreated;

import java.util.Comparator;

public final class ComparatorByCreated implements Comparator<IHasCreated> {

    private final static ComparatorByCreated INSTANCE = new ComparatorByCreated();

    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasCreated o1, final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateCreate().compareTo(o2.getDateCreate());
    }

}
