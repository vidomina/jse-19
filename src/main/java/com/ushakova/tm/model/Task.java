package com.ushakova.tm.model;

import com.ushakova.tm.api.entity.IWBS;
import com.ushakova.tm.enumerated.Status;

import java.util.Date;

public class Task extends AbstractEntity implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date dateStart;

    private Date dateFinish;

    private Date dateCreate = new Date();

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Id: " + this.getId() + "\nTitle: " + name
                + "\nDescription: " + description
                + "\nStatus: " + status.getDisplayName()
                + "\nStart Date:" + dateStart
                + "\nCreated: " + dateCreate;
    }

}
