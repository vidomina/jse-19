package com.ushakova.tm.bootstrap;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.*;
import com.ushakova.tm.command.AbstractCommand;
import com.ushakova.tm.command.auth.*;
import com.ushakova.tm.command.project.*;
import com.ushakova.tm.command.system.*;
import com.ushakova.tm.command.task.*;
import com.ushakova.tm.command.user.*;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.system.UnknownCommandException;
import com.ushakova.tm.repository.CommandRepository;
import com.ushakova.tm.repository.ProjectRepository;
import com.ushakova.tm.repository.TaskRepository;
import com.ushakova.tm.repository.UserRepository;
import com.ushakova.tm.service.*;
import com.ushakova.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new TaskFindOneByIdCommand());
        registry(new TaskFindOneByIndexCommand());
        registry(new TaskFindOneByNameCommand());
        registry(new TaskUpdateTaskByIdCommand());
        registry(new TaskUpdateTaskByIndexCommand());
        registry(new TaskStartTaskByIdCommand());
        registry(new TaskStartTaskByIndexCommand());
        registry(new TaskStartTaskByNameCommand());
        registry(new TaskCompleteTaskByIdCommand());
        registry(new TaskCompleteTaskByIndexCommand());
        registry(new TaskCompleteTaskByNameCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskRemoveOneByIdCommand());
        registry(new TaskRemoveOneByIndexCommand());
        registry(new TaskRemoveOneByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new ProjectRemoveOneByNameCommand());
        registry(new ProjectRemoveOneByIdCommand());
        registry(new ProjectRemoveOneByIndexCommand());
        registry(new ProjectFindOneByIdCommand());
        registry(new ProjectFindOneByIndexCommand());
        registry(new ProjectFindOneByNameCommand());
        registry(new ProjectUpdateProjectByIdCommand());
        registry(new ProjectUpdateProjectByIndexCommand());
        registry(new ProjectStartProjectByIdCommand());
        registry(new ProjectStartProjectByIndexCommand());
        registry(new ProjectStartProjectByNameCommand());
        registry(new ProjectCompleteProjectByIdCommand());
        registry(new ProjectCompleteProjectByIndexCommand());
        registry(new ProjectCompleteProjectByNameCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowListCommand());

        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthShowProfileInfoCommand());
        registry(new AuthUpdateProfileCommand());
        registry(new AuthSetPasswordCommand());
        registry(new UserFindByIdCommand());
        registry(new UserFindByLoginCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserShowListCommand());

        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());
        registry(new ArgumentsListCommand());
        registry(new CommandsListCommand());
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    private void initData() {
        projectService.add("ProjectName", "My Description").setStatus(Status.COMPLETE);
        projectService.add("_ProjectName2", "My Description2").setStatus(Status.IN_PROGRESS);
        projectService.add("AProjectName3", "My Description3").setStatus(Status.NOT_STARTED);
        taskService.add("Task1", "My Task Description").setStatus(Status.COMPLETE);
        taskService.add("_64TaskName2067", "My Description27").setStatus(Status.IN_PROGRESS);
        taskService.add("ATaskName3", "My Description3").setStatus(Status.NOT_STARTED);
    }

    private void initUsers() {
        getUserService().add("Guest", "Guest", "wholah0@baidu.com");
        getUserService().add("Admin", "Admin", Role.ADMIN);
    }

    public void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setIServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        loggerService.debug("<<Debug Message>>");
        loggerService.info("*   Welcome To Task Manager   *");
        if (parseArgs(args)) System.exit(0);
        initData();
        initUsers();
        while (true) {
            System.out.println("***Enter Command: ");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[Ok]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[Fail]");
            }
        }
    }

}
