package com.ushakova.tm.util;

import com.ushakova.tm.exception.system.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String line = nextLine();
        try {
            return Integer.parseInt(line);
        } catch (Exception e) {
            throw new IncorrectIndexException(line);
        }
    }

}
