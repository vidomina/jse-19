package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;

public class TaskCompleteTaskByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Set \"Complete\" status to task by id.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Set Status \"Complete\" to Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().completeTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "complete-task-by-id";
    }

}
