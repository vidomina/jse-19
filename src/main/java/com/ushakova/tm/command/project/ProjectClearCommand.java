package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all projects.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("*Project Clear*");
        serviceLocator.getProjectService().clear();
    }

    @Override
    public String name() {
        return "project-clear";
    }
}
