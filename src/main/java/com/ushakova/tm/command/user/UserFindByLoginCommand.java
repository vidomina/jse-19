package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.TerminalUtil;

public class UserFindByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Enter user login:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        System.out.println(user);
    }

    @Override
    public String name() {
        return "user-find-by-login";
    }

}
