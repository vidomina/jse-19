package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.service.ITaskService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.empty.EmptyDescriptionException;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.exception.system.IncorrectIndexException;
import com.ushakova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task completeTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task completeTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task completeTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task startTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
