package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.service.IProjectService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.empty.EmptyDescriptionException;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.exception.system.IncorrectIndexException;
import com.ushakova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project completeProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project completeProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project completeProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project startProjectById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
