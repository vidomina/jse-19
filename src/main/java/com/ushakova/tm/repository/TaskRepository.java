package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> projects = new ArrayList<>(entities);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : entities) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(String name) {
        final Task task = findOneByName(name);
        if (name == null) return null;
        remove(task);
        return task;
    }

}
