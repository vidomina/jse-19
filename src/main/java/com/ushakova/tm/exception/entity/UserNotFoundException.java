package com.ushakova.tm.exception.entity;

import com.ushakova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("An error has occurred: user was not found.");
    }

}

