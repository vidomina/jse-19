package com.ushakova.tm.api.controller;

public interface IProjectController {

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

    void clear();

    void completeProjectById();

    void completeProjectByIndex();

    void completeProjectByName();

    void create();

    void findOneById();

    void findOneByIndex();

    void findOneByName();

    void removeOneById();

    void removeOneByIndex();

    void removeOneByName();

    void showList();

    void startProjectById();

    void startProjectByIndex();

    void startProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

}
