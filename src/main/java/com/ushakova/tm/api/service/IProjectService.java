package com.ushakova.tm.api.service;

import com.ushakova.tm.api.IService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    Project add(String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

    Project completeProjectById(String id);

    Project completeProjectByIndex(Integer index);

    Project completeProjectByName(String name);

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

}
