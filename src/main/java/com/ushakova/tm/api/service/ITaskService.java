package com.ushakova.tm.api.service;

import com.ushakova.tm.api.IService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task add(String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task completeTaskById(String id);

    Task completeTaskByIndex(Integer index);

    Task completeTaskByName(String name);

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

}
